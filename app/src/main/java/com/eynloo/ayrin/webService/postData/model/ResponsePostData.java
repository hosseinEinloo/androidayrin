
package com.eynloo.ayrin.webService.postData.model;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ResponsePostData implements Serializable
{

    @SerializedName("channel_id")
    @Expose
    private Integer channelId;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("entry_id")
    @Expose
    private Integer entryId;
    @SerializedName("field1")
    @Expose
    private Object field1;
    @SerializedName("field2")
    @Expose
    private String field2;
    @SerializedName("field3")
    @Expose
    private Object field3;
    @SerializedName("field4")
    @Expose
    private Object field4;
    @SerializedName("field5")
    @Expose
    private Object field5;
    @SerializedName("field6")
    @Expose
    private Object field6;
    @SerializedName("field7")
    @Expose
    private Object field7;
    @SerializedName("field8")
    @Expose
    private Object field8;
    @SerializedName("latitude")
    @Expose
    private Object latitude;
    @SerializedName("longitude")
    @Expose
    private Object longitude;
    @SerializedName("elevation")
    @Expose
    private Object elevation;
    @SerializedName("status")
    @Expose
    private Object status;
    private final static long serialVersionUID = 349343874786078109L;

    public Integer getChannelId() {
        return channelId;
    }

    public void setChannelId(Integer channelId) {
        this.channelId = channelId;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public Integer getEntryId() {
        return entryId;
    }

    public void setEntryId(Integer entryId) {
        this.entryId = entryId;
    }

    public Object getField1() {
        return field1;
    }

    public void setField1(Object field1) {
        this.field1 = field1;
    }

    public String getField2() {
        return field2;
    }

    public void setField2(String field2) {
        this.field2 = field2;
    }

    public Object getField3() {
        return field3;
    }

    public void setField3(Object field3) {
        this.field3 = field3;
    }

    public Object getField4() {
        return field4;
    }

    public void setField4(Object field4) {
        this.field4 = field4;
    }

    public Object getField5() {
        return field5;
    }

    public void setField5(Object field5) {
        this.field5 = field5;
    }

    public Object getField6() {
        return field6;
    }

    public void setField6(Object field6) {
        this.field6 = field6;
    }

    public Object getField7() {
        return field7;
    }

    public void setField7(Object field7) {
        this.field7 = field7;
    }

    public Object getField8() {
        return field8;
    }

    public void setField8(Object field8) {
        this.field8 = field8;
    }

    public Object getLatitude() {
        return latitude;
    }

    public void setLatitude(Object latitude) {
        this.latitude = latitude;
    }

    public Object getLongitude() {
        return longitude;
    }

    public void setLongitude(Object longitude) {
        this.longitude = longitude;
    }

    public Object getElevation() {
        return elevation;
    }

    public void setElevation(Object elevation) {
        this.elevation = elevation;
    }

    public Object getStatus() {
        return status;
    }

    public void setStatus(Object status) {
        this.status = status;
    }

}
