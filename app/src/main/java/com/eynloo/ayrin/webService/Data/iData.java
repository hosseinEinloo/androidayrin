package com.eynloo.ayrin.webService.Data;

import com.eynloo.ayrin.webService.Data.model.ResponseData;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface iData {


    void doGetData();

    interface iResult {
        void onSuccessGetData(ResponseData responseData);
        void onFailedGetData(int errorId, String ErrorMessage);
    }

    interface apiRequest {

        @GET("channels/1042764/feeds.json")
        Call<ResponseData> getData();
    }

}
