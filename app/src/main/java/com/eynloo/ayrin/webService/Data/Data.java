package com.eynloo.ayrin.webService.Data;

import com.eynloo.ayrin.utils.ErrorMessage;
import com.eynloo.ayrin.webService.ApiClient;
import com.eynloo.ayrin.webService.Data.model.ResponseData;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Data implements iData {


    private iResult iResult;

    public Data() {
    }

    public Data(iData.iResult iResult) {
        this.iResult = iResult;
    }


    //Response<ResponseBanner>


    @Override
    public void doGetData() {
        Call<ResponseData> call = new ApiClient().getClient().create(apiRequest.class).getData();
        call.enqueue(new Callback<ResponseData>() {
            @Override
            public void onResponse(Call<ResponseData> call, Response<ResponseData> response) {
                if (response.code() == 200) {

                    if (iResult != null) {
                        iResult.onSuccessGetData(response.body());
                    }

                } else {
                    if (iResult != null) {
                        iResult.onFailedGetData(response.code(), ErrorMessage.getErrorByCode(response.code()));

                    }

                }
            }

            @Override
            public void onFailure(Call<ResponseData> call, Throwable t) {
                if (iResult != null && !call.isCanceled()) {
                    iResult.onFailedGetData(0, ErrorMessage.ERROR_NETWORK_UNAVALABLE.getErrorMessage());
                }
            }
        });
    }
}
