package com.eynloo.ayrin.webService.postData;

import com.eynloo.ayrin.webService.Data.model.ResponseData;
import com.eynloo.ayrin.webService.postData.model.ResponsePostData;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface iPostData {

    void doPostData(String api_key,int field2);

    interface iResult {
        void onSuccessPostData(ResponsePostData responsePostData);
        void onFailedPostData(int errorId, String ErrorMessage);
    }

    interface apiRequest {

        @POST("update.json")
        Call<ResponsePostData> getData(@Query("api_key") String api_key,@Query("field2") int field2);
    }

}
