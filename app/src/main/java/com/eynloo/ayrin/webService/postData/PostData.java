package com.eynloo.ayrin.webService.postData;

import com.eynloo.ayrin.utils.ErrorMessage;
import com.eynloo.ayrin.webService.ApiClient;
import com.eynloo.ayrin.webService.Data.iData;
import com.eynloo.ayrin.webService.Data.model.ResponseData;
import com.eynloo.ayrin.webService.postData.model.ResponsePostData;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PostData implements iPostData {



    private iPostData.iResult iResult;

    public PostData() {
    }

    public PostData(iPostData.iResult iResult) {
        this.iResult = iResult;
    }


    //Response<ResponseBanner>


    @Override
    public void doPostData(String api_key,int field) {
        Call<ResponsePostData> call = new ApiClient().getClient().create(PostData.apiRequest.class).getData(api_key, field);
        call.enqueue(new Callback<ResponsePostData>() {
            @Override
            public void onResponse(Call<ResponsePostData> call, Response<ResponsePostData> response) {
                if (response.code() == 200) {

                    if (iResult != null) {
                        iResult.onSuccessPostData(response.body());
                    }

                } else {
                    if (iResult != null) {
                        iResult.onFailedPostData(response.code(), ErrorMessage.getErrorByCode(response.code()));

                    }

                }
            }

            @Override
            public void onFailure(Call<ResponsePostData> call, Throwable t) {
                if (iResult != null && !call.isCanceled()) {
                    iResult.onFailedPostData(0, ErrorMessage.ERROR_NETWORK_UNAVALABLE.getErrorMessage());
                }
            }
        });
    }

}
