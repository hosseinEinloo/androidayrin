package com.eynloo.ayrin.view.mainActivity;

import android.content.Context;

import com.eynloo.ayrin.webService.Data.model.Feed;
import com.eynloo.ayrin.webService.Data.model.ResponseData;
import com.eynloo.ayrin.webService.postData.model.ResponsePostData;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class P_mainActivity implements iP_mainActivity {

    M_mainActivity m_mainActivity;
    private com.eynloo.ayrin.view.mainActivity.iV_mainActivity iV_mainActivity;

    public P_mainActivity(iV_mainActivity iV_mainActivity) {
        this.iV_mainActivity = iV_mainActivity;
        m_mainActivity = new M_mainActivity(this);
    }

    @Override
    public void sendRequestToGetData() {
        m_mainActivity.sendRequestToGetData();
    }

    @Override
    public void onSuccessGetData(ResponseData responseData,Observable observableData) {

        Observable<List<String>> observable=observableData;
        observable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(iV_mainActivity.onSuccessGetData(responseData));


    }

    @Override
    public void onFailedGetData(int errorId, String ErrorMessage) {
        iV_mainActivity.onFailedGetData(errorId, ErrorMessage);
    }


    @Override
    public Context getContext() {
        return iV_mainActivity.getContext();
    }

    @Override
    public void sendData(String api_key, int number) {
        m_mainActivity.sendData(api_key, number);
    }

    @Override
    public void onSuccessPostData(ResponsePostData responsePostData) {
        iV_mainActivity.onSuccessPostData(responsePostData);
    }

    @Override
    public void onFailedPostData(int errorId, String ErrorMessage) {
        iV_mainActivity.onFailedPostData(errorId, ErrorMessage);
    }





}
