package com.eynloo.ayrin.view.internetCheck;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.eynloo.ayrin.connection.ConnectionCheck;
import com.eynloo.ayrin.R;
import com.eynloo.ayrin.view.mainActivity.V_mainActivity;

public class InternetWarning extends AppCompatActivity {

    Button btnRetry;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_internet_warning);
        init();

        btnRetry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (ConnectionCheck.connectionState(InternetWarning.this)){
                    startActivity(new Intent(InternetWarning.this, V_mainActivity.class));

                }else {

                    Toast.makeText(InternetWarning.this, "لطفا wifi خود را چک کنید", Toast.LENGTH_SHORT).show();
                }

            }
        });



    }

    private void init() {
        btnRetry=findViewById(R.id.btn_retry);
    }

    @Override
    protected void onStop() {
        super.onStop();
        InternetWarning.this.finish();
    }
}
