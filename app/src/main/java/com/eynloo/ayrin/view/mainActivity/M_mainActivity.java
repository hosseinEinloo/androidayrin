package com.eynloo.ayrin.view.mainActivity;

import android.content.Context;

import com.eynloo.ayrin.webService.Data.Data;
import com.eynloo.ayrin.webService.Data.iData;
import com.eynloo.ayrin.webService.Data.model.Feed;
import com.eynloo.ayrin.webService.Data.model.ResponseData;
import com.eynloo.ayrin.webService.postData.PostData;
import com.eynloo.ayrin.webService.postData.iPostData;
import com.eynloo.ayrin.webService.postData.model.ResponsePostData;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;


public class M_mainActivity implements iM_mainActivity {

    iP_mainActivity iP_mainActivity;

    List<Feed> feeds = new ArrayList<>();
    List<String> strList=new ArrayList<>();

    ResponseData responseData;

    public M_mainActivity(iP_mainActivity iP_mainActivity) {
        this.iP_mainActivity = iP_mainActivity;
    }


    public void sendRequestToGetData() {

        new Data(new iData.iResult() {


            @Override
            public void onSuccessGetData(ResponseData responseData) {
                M_mainActivity.this.responseData = responseData;
//                iP_mainActivity.onSuccessGetData(responseData);
                feeds = responseData.getFeeds();
                strList.clear();
                for (int i = 0; i <feeds.size() ; i++) {
                    strList.add(responseData.getFeeds().get(i).getField2());
                }


                listNumbers();

            }

            @Override
            public void onFailedGetData(int errorId, String ErrorMessage) {
                iP_mainActivity.onFailedGetData(errorId, ErrorMessage);
            }
        }).doGetData();
    }


    @Override
    public void sendData(String api_key, int number) {
        new PostData(new iPostData.iResult() {
            @Override
            public void onSuccessPostData(ResponsePostData responsePostData) {

                iP_mainActivity.onSuccessPostData(responsePostData);

            }

            @Override
            public void onFailedPostData(int errorId, String ErrorMessage) {

                iP_mainActivity.onFailedPostData(errorId, ErrorMessage);

            }
        }).doPostData(api_key, number);
    }


    @Override
    public Context getContext() {
        return iP_mainActivity.getContext();
    }


    public void listNumbers(){
        final Observable<List<String>> observable;

        observable=Observable.just(strList);

            iP_mainActivity.onSuccessGetData(responseData,observable);
    }

}
