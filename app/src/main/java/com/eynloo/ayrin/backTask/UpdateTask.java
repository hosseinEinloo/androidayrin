package com.eynloo.ayrin.backTask;

import android.os.AsyncTask;
import android.widget.Toast;

import com.eynloo.ayrin.App;
import com.eynloo.ayrin.view.mainActivity.P_mainActivity;

public class UpdateTask extends AsyncTask<Void,Void,Void> {

    private P_mainActivity p_mainActivity;

    public UpdateTask(P_mainActivity p_mainActivity) {
        this.p_mainActivity = p_mainActivity;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected Void doInBackground(Void... voids) {
        p_mainActivity.sendRequestToGetData();
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        Toast.makeText(App.getContext(), "به روز رسانی", Toast.LENGTH_SHORT).show();
    }
}
