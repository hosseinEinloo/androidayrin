package com.eynloo.ayrin.connection;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class ConnectionCheck {


    public static boolean connectionState(Context context) {
        boolean connected = false;
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo ni = cm.getActiveNetworkInfo();
        if (ni != null) {
            if (ni.getType() == ConnectivityManager.TYPE_WIFI) {
                connected = true;
            } else if (ni.getType() == ConnectivityManager.TYPE_MOBILE) {
                connected = true;
            } else {
                connected = false;
            }
        }
        return connected;
    }

}
