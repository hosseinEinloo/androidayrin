package com.eynloo.ayrin;

import android.app.Application;
import android.content.Context;

public class App extends Application {

    protected static Context context = null;


    @Override
    public void onCreate() {
        super.onCreate();
        context = getApplicationContext();
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
    }

    public static Context getContext() {
        return context;
    }


}
